## 变量作用域与解构赋值
如果一个变量在函数体内部申明，则该变量的作用域为整个函数体，在函数体外不可引用该变量

如果两个不同的函数各自申明了同一个变量，那么该变量只在各自的函数体内起作用。换句话说，不同函数内部的同名变量互相独立，互不影响
```
'use strict';

function foo() {
    var x = 1;
    x = x + 1;
}

x = x + 2; // ReferenceError! 无法在函数体外引用变量x
```
```
'use strict';
function foo() {
    var x = 1;
    function bar() {
        var x = 'A';
        console.log('x in bar() = ' + x); // 'A'
    }
    console.log('x in foo() = ' + x); // 1
    bar();
}

foo();
这说明JavaScript的函数在查找变量时从自身函数定义开始，从“内”向“外”查找。如果内部函数定义了与外部函数重名的变量，则内部函数的变量将“屏蔽”外部函数的变量。
```
## 变量提升
```
'use strict';

function foo() {
    var x = 'Hello, ' + y;
    console.log(x);
    var y = 'Bob';
}

foo();//Hello,underfined
```
因为js中会把申明的变量提升到函数底部，但不会提升变量的赋值，所以在js引擎看来的代码相当于
对于上述foo()函数，JavaScript引擎看到的代码相当于：
```
function foo() {
    var y; // 提升变量y的申明，此时y为undefined
    var x = 'Hello, ' + y;
    console.log(x);
    y = 'Bob';
}
```
## 全局作用域
js中默认的全局对象是window，所定义的函数会被绑定到window对象中，所以可以通过window.xx直接调用
```
function foo() {
    alert('foo');
}
foo(); // 直接调用foo()
window.foo(); // 通过window.foo()调用
```