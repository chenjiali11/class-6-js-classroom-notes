### 局部作用域
局部作用域：声明在函数体内的变量，在整个函数执行环境和其子函数内都是可用的，但是在函数外访问不到，所以叫局部作用域，例如js中的for循环等语句块是无法定义具有局部作用域的变量的。
### 常量
ES6标准引入了新的关键字const来定义常量，const与let都具有块级作用域：
```
'use strict';

const PI = 3.14;
PI = 3; // 某些浏览器不报错，但是无效果！
PI; // 3.14
```
### let、var、const异同
同： 都可以声明变量。

异：

1. var 存在局部作用域，可变量提升，声明的值可更改。
2. let 存在块级作用域，不可变量提升，声明的值可修改。（只可以先声明变量，然后再使用）
3. const 存在块级作用域，不可变量提升，声明的值本身不可修改（只可以先声明变量，然后再使用）
### 建设域名访问网站
```
    要建设一个能使用域名访问的网站，需要走通如下条件：
    1.得有一个公网的IP地址，可以在网络上访问到
    2.得有一个域名，域名经过了备案（工信部备案、公安备案）
    3.域名和IP地址绑定，这个过程也叫域名解析
    4.服务器上有web服务器（nginx、apache、tomcat、IIS、Canndy）

        apt install nginx -y

        systemctl status nginx //命令用于查看nginx服务的运行状态
        systemctl start nginx  // 启动nginx服务
        systemctl stop nginx   // 停止nignx服务
        systemctl enable nginx // 将nginx服务设置为开机自动启动
        systemctl disable nginx //将nginx服务设置为禁止开机自启动

    5.服务器上有至少一个web页面（一般是首页index）
        // 网页文件所有目录:/var/www/demo6.9ihub.com/index.html

        scp .\index.html root@demo6.9ihub.com:/var/www/demo6.9ihub.com // 这个命令用于上传本地当前路径下的index.html文件
        到主机demo6.9ihub.com下的指定目录，这个指定的目录为：/var/www/demo6.9ihub.com
    6.为网站创建配置文件（后面以nginx配置文件为主作讲述）
        // 配置文件放在/etc/nginx/conf.d/demo6.9ihub.com.conf

        server {
            listen 80;
            server_name demo6.9ihub.com;

            location / {
                root /var/www/demo6.9ihub.com;
                index index.html;
            }

        }

        nginx -t // 用于测试nginx配置文件有没有语法错误
        nginx -s reload // 这个命令用于在不重启nginx的情况下，重新加载配置文件（以让新的配置文件生效，一般用于确认配置文件没有问题后执行）
    7.云服务器安全组设置，允许访问80端口（如果通过80端口访问的话，也可以是其它端口号）

```