## resolve()
Promise.resolve() 方法用于解决（resolve）一个 Promise 对象，简而言之，Promise.resolve() 返回一个 Promise 对象，其最终状态取决于另一个 Promise 对象、thenable 对象或其他值。
## promise
当通过new创建Promise实例时，需要传入一个回调函数，我们称之为executor

这个回调函数会被立刻执行，并传入两个回调参数resolve、reject

当调用resolve回调函数时，会执行 Promise 对象的then方法传入的回调

当调用reject回调函数时，会执行 Promise 对象的catch方法传入的回调
