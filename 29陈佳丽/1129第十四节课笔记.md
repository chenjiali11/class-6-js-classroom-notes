### 练习1
1. 实现栈结构
```js
function Stack(){
    const items = [];
    this.push = item=>{
        items.push(item); // 这里通过闭包访问item
    }
    this.pop = ()=>items.pop();// 这里通过闭包访问item
}
let stack = new Stack();
stack.items // undefined
```
### 练习2
简单乘法，实现方法mult，可接受1~2个参数

1. 若参数中不包含回调函数，则返回一个包含mult方法的对象

2. 若参数中包含回调函数，则执行回调函数

3. 不需要考虑传入参数类型异常的情况
```js
function mult (num){
    function innerMult(nextNum,callback){
        const result = num * nextNum;
    }
    if(callback){
        callback(result)
    }
    return mult:innerMult,
    getResult:()=>num
} 
```